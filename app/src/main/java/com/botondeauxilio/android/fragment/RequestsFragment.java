package com.botondeauxilio.android.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.ContactManageActivity;
import com.botondeauxilio.android.adapter.AcceptRequestListAdapter;
import com.botondeauxilio.android.models.RequestModel;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;
import static com.google.firebase.firestore.Query.Direction.DESCENDING;

public class RequestsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();

    ContactManageActivity activity;

    @BindView(R.id.swipe_layout) SwipeRefreshLayout swipe_layout;
    @BindView(R.id.lst_request) ListView lst_request;
    AcceptRequestListAdapter adapter;
    ArrayList<UserModel> allRequests = new ArrayList<>();
    ArrayList<RequestModel> receiverIDs = new ArrayList<>();

    boolean expanded = false;
    Drawable less, more;
    @BindView(R.id.txv_help) TextView txv_help;

    public RequestsFragment(ContactManageActivity activity) {
        this.activity = activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_requests, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        less = getResources().getDrawable(R.drawable.ic_expand_less);
        less.setBounds(0, 0, 60, 60);
        more = getResources().getDrawable(R.drawable.ic_expand_more);
        more.setBounds(0, 0, 60, 60);
        swipe_layout.setOnRefreshListener(this);

        adapter = new AcceptRequestListAdapter(activity, this);
        lst_request.setAdapter(adapter);

        swipe_layout.post(new Runnable() {
            @Override
            public void run() {
                getAllRequests();
            }
        });
    }

    @OnClick(R.id.txv_help) void showHelp(){

        expanded = !expanded;
        if (expanded){
            txv_help.setText(getString(R.string.help_request));
            txv_help.setCompoundDrawables(null, null, less, null);
        }
        else {
            txv_help.setText(getString(R.string.in_this_tab));
            txv_help.setCompoundDrawables(null, null, more, null);
        }
    }

    private void getAllRequests(){

        receiverIDs.clear();
        allRequests.clear();

        swipe_layout.setRefreshing(true);

        db.collection("users").document(g_user.id).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){

                            DocumentSnapshot document = task.getResult();

                            List<Map<String, Object>> maps = new ArrayList<>();

                            if (document.getData().containsKey("receivedID")){

                                maps = (List<Map<String, Object>>) document.get("receivedID");

                            }

                            for (int i = 0; i < maps.size(); i++){

                                RequestModel model = new RequestModel();
                                model.receiverID = maps.get(i).get("receiverID").toString();
                                model.timeStamp = maps.get(i).get("timeStamp").toString();

                                receiverIDs.add(model);
                            }

                            g_user.receivedID = receiverIDs;

                            db.collection("users")
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                            if (task.isSuccessful()){

                                                swipe_layout.setRefreshing(false);

                                                for (QueryDocumentSnapshot document : task.getResult()){

                                                    for (int i = 0; i < g_user.receivedID.size(); i++){

                                                        if (g_user.receivedID.get(i).receiverID.equals(document.getId())){

                                                            String timeStamp = g_user.receivedID.get(i).timeStamp;

                                                            String fcmToken = document.getData().get("fcmToken").toString();

                                                            UserModel userModel = new UserModel(
                                                                    receiverIDs.get(i).receiverID,
                                                                    document.getData().get("first_name").toString(),
                                                                    document.getData().get("last_name").toString(),
                                                                    document.getData().get("email").toString(),
                                                                    document.getData().get("idNumber").toString(),
                                                                    fcmToken,
                                                                    document.getData().get("photoUrl").toString(),
                                                                    timeStamp);

                                                            allRequests.add(userModel);
                                                        }
                                                    }

                                                }

                                                Collections.sort(allRequests, new Comparator<UserModel>() {
                                                    @Override
                                                    public int compare(UserModel o1, UserModel o2) {
                                                        return o2.timeStamp.compareTo(o1.timeStamp);
                                                    }
                                                });

                                                adapter.setList(allRequests);
                                            }
                                        }
                                    });
                        }

                    }
                });

    }

    public void acceptRequest(UserModel accept){

        activity.showHUD();
        db.collection("users").document(accept.id).collection("requests")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot document : task.getResult()){

                                if(document.get("receiverID").equals(g_user.id)){

                                    db.collection("users").document(accept.id).collection("requests")
                                            .document(document.getId()).update("status", true);

                                    ArrayList<Map> arrayMap = new ArrayList<>();

                                    for (int i = 0; i < g_user.receivedID.size(); i++){

                                        if (g_user.receivedID.get(i).receiverID.equalsIgnoreCase(accept.id))
                                            continue;

                                        Map<String, Object> map = new HashMap<>();
                                        map.put("receiverID", g_user.receivedID.get(i).receiverID);
                                        map.put("status", false);
                                        map.put("timeStamp", g_user.receivedID.get(i).timeStamp);

                                        arrayMap.add(map);
                                    }
                                    db.collection("users").document(g_user.id).update("receivedID", arrayMap);

                                    sendNotifi(accept, getString(R.string.request_accept));
                                }
                            }
                        }
                    }
                });

    }

    public void rejectRequest(UserModel reject){

        activity.showHUD();

        db.collection("users").document(reject.id).collection("requests")
                .whereEqualTo("receiverID", g_user.id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot document : task.getResult()){

                                db.collection("users").document(reject.id).collection("requests").document(document.getId()).delete();

                                ArrayList<Map> arrayMap = new ArrayList<>();

                                for (int i = 0; i < g_user.receivedID.size(); i++){

                                    if (g_user.receivedID.get(i).receiverID.equalsIgnoreCase(reject.id))
                                        continue;

                                    Map<String, Object> map = new HashMap<>();
                                    map.put("receiverID", g_user.receivedID.get(i).receiverID);
                                    map.put("status", false);
                                    map.put("timeStamp", g_user.receivedID.get(i).timeStamp);

                                    arrayMap.add(map);
                                }
                                db.collection("users").document(g_user.id).update("receivedID", arrayMap);

                                sendNotifi(reject, getString(R.string.request_reject));

                            }
                        }
                    }
                });
    }

    private Task<String> sendNotifi(UserModel user, String body){

        ArrayList<String> receiverIDs = new ArrayList<>();
        receiverIDs.add(user.id);

        Map<String, Object> data = new HashMap<>();
        data.put("title", g_user.getName());
        data.put("body", g_user.getName() + body);
        data.put("receiverID", receiverIDs);
        data.put("senderID", g_user.id);
        data.put("sound","noti_sound.mp3");
        data.put("type", "addContact");
        data.put("status", "normal");

        return mFunctions
                .getHttpsCallable("sendNotification")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.

                        //String result = (String) task.getResult().getData();

                        activity.hideHUD();

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                allRequests.remove(user);
                                adapter.setList(allRequests);
                            }
                        });

                        return "";
                    }
                });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ContactManageActivity)context;
    }

    @Override
    public void onRefresh() {
        getAllRequests();
    }

    @Override
    public void onResume() {
        super.onResume();
        expanded = false;
    }
}
