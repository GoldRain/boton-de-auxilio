package com.botondeauxilio.android.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.ContactManageActivity;
import com.botondeauxilio.android.adapter.AddContactListAdapter;
import com.botondeauxilio.android.models.RequestModel;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class AddContactFragment extends Fragment{

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();

    ContactManageActivity activity;
    AddContactListAdapter adapter;
    ArrayList<UserModel> allUsers = new ArrayList<>();
    @BindView(R.id.txv_help) TextView txv_help;
    @BindView(R.id.lst_add_contact) ListView lst_add_contact;

    @BindView(R.id.imv_search) ImageView imv_searchIcon;
    @BindView(R.id.edt_search) EditText edt_search;

    boolean expanded = false;
    Drawable less, more;

    public AddContactFragment(ContactManageActivity activity) {

        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_contact, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        less = getResources().getDrawable(R.drawable.ic_expand_less);
        less.setBounds(0, 0, 60, 60);
        more = getResources().getDrawable(R.drawable.ic_expand_more);
        more.setBounds(0, 0, 60, 60);


        adapter = new AddContactListAdapter(activity, this);
        lst_add_contact.setAdapter(adapter);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0){
                    imv_searchIcon.setImageResource(R.drawable.ic_cancel);
                }else imv_searchIcon.setImageResource(R.drawable.ic_search);
            }

            @Override
            public void afterTextChanged(Editable s) {

                String str_search = edt_search.getText().toString();
                adapter.searchContact(str_search);
            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        imv_searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_search.setText("");
            }
        });

     getAllUsers();

    }

    @OnClick(R.id.txv_help) void showHelp(){
        expanded = !expanded;
        if (expanded){
            txv_help.setText(getString(R.string.help_add));
            txv_help.setCompoundDrawables(null, null, less, null);
        }
        else {
            txv_help.setText(getString(R.string.in_this_tab));
            txv_help.setCompoundDrawables(null, null, more, null);
        }
    }

    private void getAllUsers(){

        allUsers.clear();

        db.collection("users").document(g_user.id).collection("requests")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){

                            ArrayList<RequestModel> arrayReq = new ArrayList<>();

                            for (QueryDocumentSnapshot document : task.getResult()){

                                Boolean status = (boolean)document.get("status");
                                RequestModel requestModel = new RequestModel(
                                        document.get("receiverID").toString(),
                                        status,
                                        document.get("timeStamp").toString());

                                arrayReq.add(requestModel);
                            }

                            g_user.requestedID = arrayReq;

                        }
                    }
                });

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){


                            for (QueryDocumentSnapshot document : task.getResult()){

                                if (g_user.id.equals(document.getId())){
                                    continue;
                                }

                                String fcmToken = Prefs.getString(Constants.TOKEN,"");
                                if(fcmToken.isEmpty()){

                                    if (document.getData().containsKey("fcmToken")) {
                                        fcmToken = document.getData().get("fcmToken").toString();
                                    }
                                }

                                ArrayList<String> contactID = new ArrayList<>();
                                if (document.getData().containsKey("contactID")){
                                    contactID = (ArrayList<String>)document.getData().get("contactID");
                                }

                                ArrayList<RequestModel> receivedID = new ArrayList<>();
                                if (document.getData().containsKey("receivedID")){
                                    receivedID = (ArrayList<RequestModel>) document.getData().get("receivedID");
                                }

                                ArrayList<RequestModel> requestedID = new ArrayList<>();
                                if (document.getData().containsKey("requestedID")){
                                    requestedID = (ArrayList<RequestModel>) document.getData().get("requestedID");
                                }

                                UserModel userModel = new UserModel(
                                        document.getId(),
                                        document.getData().get("first_name").toString(),
                                        document.getData().get("last_name").toString(),
                                        document.getData().get("email").toString(),
                                        document.getData().get("idNumber").toString(),
                                        document.getData().get("phoneNumber").toString(),
                                        fcmToken,
                                        contactID,
                                        document.getData().get("photoUrl").toString(),
                                        receivedID,
                                        requestedID);

                                allUsers.add(userModel);
                            }

                            adapter.setList(allUsers);
                        }
                    }
                });

    }
    public void addContactRequest(UserModel request){

        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();

        Map<String, Object> requestData = new HashMap<>();
        requestData.put("receiverID", request.id);
        requestData.put("status", false);
        requestData.put("timeStamp", ts);

        for (int i = 0; i < g_user.requestedID.size(); i++){
            if(g_user.requestedID.get(i).receiverID.equalsIgnoreCase(request.id)){
                activity.showToast(getString(R.string.already_sent_request));
                return;
            }
        }

        activity.showHUD();
        // Add a new document with a generated ID to another user's receiver
        db.collection("users").document(g_user.id).collection("requests").document()
                .set(requestData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        RequestModel aa = new RequestModel(g_user.id, false, ts);

                        request.receivedID.add(aa);

                        db.collection("users").document(request.id)
                                .update("receivedID", request.receivedID)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        RequestModel bb = new RequestModel(request.id, ts);

                                        g_user.requestedID.add(bb);

                                        //send push notification.
                                        sendAddNotification(request);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

    }

    private Task<String> sendAddNotification(UserModel user){

        ArrayList<String> receiverIDs = new ArrayList<>();

        receiverIDs.add(user.id);

        Map<String, Object> data = new HashMap<>();
        data.put("title", g_user.getName());
        data.put("body", g_user.getName() + " " + getString(R.string.sent_request));
        data.put("receiverID", receiverIDs);
        data.put("senderID", Constants.g_user.id);
        data.put("sound","noti_sound.mp3");
        data.put("type", "addContact");
        data.put("status", "normal");

        return mFunctions
                .getHttpsCallable("sendNotification")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.

                        activity.hideHUD();

                        String result = (String) task.getResult().getData();

                        allUsers.remove(user);
                        adapter.setList(allUsers);

                        return result;
                    }
                });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ContactManageActivity)context;
    }

    @Override
    public void onResume() {
        super.onResume();
        expanded = false;
    }
}
