package com.botondeauxilio.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.adapter.ContactListAdapter;
import com.botondeauxilio.android.activities.ContactManageActivity;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.botondeauxilio.android.utils.Constants.g_user;

public class ContactsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @BindView(R.id.lst_contact)
    ListView lst_contact;
    ContactListAdapter adapter;
    ArrayList<UserModel> contacts = new ArrayList<>();
    public ArrayList<UserModel> checkedUser = new ArrayList<>();

    @BindView(R.id.swipe_layout) SwipeRefreshLayout swipe_layout;

    ContactManageActivity activity;

    public ContactsFragment(ContactManageActivity activity) {
        this.activity = activity;
    }

    boolean expanded = false;
    Drawable less, more;
    @BindView(R.id.txv_help) TextView txv_help;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contacts, container, false);

        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    public void refreshLayout(){

        lst_contact.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        adapter = new ContactListAdapter(activity, this);
        lst_contact.setAdapter(adapter);
        adapter.setList(contacts);
    }

    private void loadLayout(){

        less = getResources().getDrawable(R.drawable.ic_expand_less);
        less.setBounds(0, 0, 60, 60);
        more = getResources().getDrawable(R.drawable.ic_expand_more);
        more.setBounds(0, 0, 60, 60);

        swipe_layout.setOnRefreshListener(this);
        adapter = new ContactListAdapter(activity, this);
        lst_contact.setAdapter(adapter);

       /* edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0){
                    imv_searchIcon.setImageResource(R.drawable.ic_cancel);
                }else imv_searchIcon.setImageResource(R.drawable.ic_search);
            }

            @Override
            public void afterTextChanged(Editable s) {

                String str_search = edt_search.getText().toString();
                adapter.searchContact(str_search);
            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        imv_searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_search.setText("");
            }
        });
*/
       swipe_layout.post(new Runnable() {
           @Override
           public void run() {
               getAllContact();
           }
       });
    }

    @OnClick(R.id.txv_help) void showHelp(){

        expanded = !expanded;

        if (expanded){
            txv_help.setText(getString(R.string.help_contacts));
            txv_help.setCompoundDrawables(null, null, less, null);
        }
        else {
            txv_help.setText(getString(R.string.in_this_tab));
            txv_help.setCompoundDrawables(null, null, more, null);
        }
    }

    private void getAllContact(){

        swipe_layout.setRefreshing(true);

        ArrayList<String> contactID = new ArrayList<>();
        contacts.clear();

        db.collection("users").document(g_user.id).collection("requests").whereEqualTo("status", true)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){

                            swipe_layout.setRefreshing(false);

                            for (QueryDocumentSnapshot document : task.getResult()){

                                contactID.add(document.get("receiverID").toString());
                                Log.d("contact ID==>", document.get("receiverID").toString());
                            }

                            db.collection("users")
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            //hideHUD();
                                            if (task.isSuccessful()) {

                                                for (QueryDocumentSnapshot document : task.getResult()) {

                                                    //Log.d("ContactActivity", document.getId() + " => " + document.getData());

                                                    if (Constants.g_user.id.equals(document.getId()))
                                                        continue;

                                                    for (int j = 0; j < contactID.size(); j++){

                                                        if (contactID.get(j).equalsIgnoreCase(document.getId())){

                                                            boolean is_exist =  false;
                                                            for (int i = 0; i < g_user.contactID.size(); i++){

                                                                if (g_user.contactID.get(i).equals(document.getId())){
                                                                    is_exist = true;
                                                                    break;
                                                                }
                                                            }

                                                            if (!is_exist){

                                                                String fcmToken = "";
                                                                if (document.getData().containsKey("fcmToken")) {
                                                                    fcmToken = document.getData().get("fcmToken").toString();
                                                                }

                                                                ArrayList<String> contactID = new ArrayList<>();
                                                                if (document.getData().containsKey("contactID")){
                                                                    contactID = (ArrayList<String>)document.getData().get("contactID");
                                                                }

                                                                UserModel userModel = new UserModel(
                                                                        document.getId(),
                                                                        document.getData().get("first_name").toString(),
                                                                        document.getData().get("last_name").toString(),
                                                                        document.getData().get("email").toString(),
                                                                        document.getData().get("idNumber").toString(),
                                                                        document.getData().get("phoneNumber").toString(),
                                                                        fcmToken,
                                                                        contactID,
                                                                        document.getData().get("photoUrl").toString());

                                                                contacts.add(userModel);
                                                            }

                                                        }
                                                    }
                                                }

                                                adapter.setList(contacts);

                                            } else {
                                                swipe_layout.setRefreshing(false);
                                                activity.showToast(getString(R.string.error_document));
                                            }
                                        }
                                    });

                        }
                    }
                });


    }


    @OnClick(R.id.btn_add) void addContacts(){

        gotoSend(adapter.getCheckedContact());

    }

    private void gotoSend(ArrayList<UserModel> sender){

        if (sender.size() == 0){
            activity.showToast(getString(R.string.no_select_contact));
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_CONTACTS, sender);
        activity.setResult(RESULT_OK, intent);
        activity.finish();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ContactManageActivity)context;
    }

    @Override
    public void onRefresh() {
        getAllContact();
    }

    @Override
    public void onResume() {
        super.onResume();
        expanded = false;
    }
}
