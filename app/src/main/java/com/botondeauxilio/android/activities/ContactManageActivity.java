package com.botondeauxilio.android.activities;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ToxicBakery.viewpager.transforms.AccordionTransformer;
import com.astuetz.PagerSlidingTabStrip;
import com.botondeauxilio.android.R;
import com.botondeauxilio.android.adapter.ContactManageViewPagerAdapter;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class ContactManageActivity extends BaseActivity {

    ContactManageViewPagerAdapter adapterViewPager;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar) AppBarLayout appbar;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.tabs) PagerSlidingTabStrip tabsStrip;
    @BindView(R.id.imv_user) CircleImageView imv_user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_manage);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        Glide.with(this).load(g_user.photoUrl).into(imv_user);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        adapterViewPager = new ContactManageViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapterViewPager);

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip.setViewPager(viewPager);
        viewPager.setPageTransformer(true, new AccordionTransformer());
    }

    @OnClick(R.id.imv_user) void gotoAccount(){

        startActivity(new Intent(ContactManageActivity.this, AccountActivity.class));
    }

    @Override
    public void onExit() {
        finish();
    }
}
