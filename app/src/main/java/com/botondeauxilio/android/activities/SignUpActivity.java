package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.botondeauxilio.android.BotondeAuxilioApplication;
import com.botondeauxilio.android.R;
import com.botondeauxilio.android.models.RequestModel;
import com.botondeauxilio.android.utils.BitmapUtils;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pixplicity.easyprefs.library.Prefs;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

import static com.kakao.usermgmt.StringSet.email;

public class SignUpActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    DocumentReference documentReference = db.collection("users").document();

    @BindView(R.id.imv_user) CircleImageView imv_user;
    @BindView(R.id.imv_edit) ImageView imv_edit;

    @BindView(R.id.edt_first_name) EditText edt_first_name;
    @BindView(R.id.edt_last_name) EditText edt_last_name;
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_id_number) EditText edt_id_number;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.txv_term) TextView txv_term;
    @BindView(R.id.checkbox) CheckBox checkbox;
    String _email = "", _password = "", _first_name = "", _last_name = "", _id_number = "",  _phoneNumber = "";

    ArrayList<String> contactID = new ArrayList<>();
    ArrayList<RequestModel> receivedID = new ArrayList<>();
    ArrayList<RequestModel> requestedID = new ArrayList<>();

    Uri _imageCaptureUri;
    String _photoPath = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();


        ButterKnife.bind(this);
        checkPermission();
        loadLayout();
    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.CAMERA, PermissionEnum.READ_EXTERNAL_STORAGE, PermissionEnum.WRITE_EXTERNAL_STORAGE,
                        PermissionEnum.ACCESS_COARSE_LOCATION, PermissionEnum.ACCESS_FINE_LOCATION)
                .askAgain(false)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){
                    }
                })
                .ask(this);
    }

    private void loadLayout(){

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });
    }

    @OnClick({R.id.txv_term, R.id.checkbox}) void checkTerms(){
        checkbox.setChecked(true);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plush.com.pe/auxilio/documentos/TERMINOS_Y_CONDICIONES_REGISTRO.pdf"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.btn_signup) void gotoMain(){

        _first_name = edt_first_name.getText().toString();
        _last_name = edt_last_name.getText().toString();
        _phoneNumber = edt_phone.getText().toString();
        _email = edt_email.getText().toString();
        _id_number = edt_id_number.getText().toString();
        _password = edt_password.getText().toString();


        if (checkedValid())
            createUser();
    }

    private void createUser() {

        showHUD();
        mAuth.createUserWithEmailAndPassword(_email, _password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            user = mAuth.getCurrentUser();

                            // Create a storage reference from our app

                            Uri file = Uri.fromFile(new File(_photoPath));

                            StorageReference riversRef = storage.getReference().child("profile/" + user.getUid());
                            UploadTask uploadTask = riversRef.putFile(file);
                            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }

                                    // Continue with the task to get the download URL
                                    return riversRef.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()) {

                                        Uri downloadUri = task.getResult();

                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("SignUpActivity", "createUserWithEmail:success");

                                        Map<String, Object> userData = new HashMap<>();
                                        userData.put("first_name", _first_name);
                                        userData.put("last_name", _last_name);
                                        userData.put("phoneNumber", _phoneNumber);
                                        userData.put("email", _email);
                                        userData.put("idNumber", _id_number);
                                        userData.put("photoUrl", downloadUri.toString());
                                        userData.put("fcmToken", Prefs.getString(Constants.TOKEN, ""));
                                        userData.put("contactID", contactID);
                                        userData.put("receivedID", receivedID);

                                        //db.collection("notifications").document().set()
                                        db.collection("users").document(user.getUid())
                                                .set(userData)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {

                                                        //send email verification

                                                        user.sendEmailVerification()
                                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                    @Override
                                                                    public void onSuccess(Void aVoid) {

                                                                        Log.d("success", "succss" + _email + _password);
                                                                        Prefs.putString(Constants.PREFKEY_USEREMAIL, _email);
                                                                        Prefs.putString(Constants.PREFKEY_USERPWD, _password);

                                                                        progressLogin();
                                                                        Log.d("SignUpActivity", "DocumentSnapshot successfully written!");

                                                                    }

                                                                })
                                                                .addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception e) {
                                                                    hideHUD();
                                                                    Log.d("result", "Failed send verification");
                                                                }
                                                            });


                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w("SignUpActivity", "Error writing document", e);
                                                    }
                                                });

                                    } else {
                                        hideHUD();
                                        showToast(getString(R.string.photo_failed));
                                    }
                                }
                            });
                        } else {
                            hideHUD();
                            // If sign in fails, display a message to the user.
                            Log.w("SignUpActivity", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


    private void progressLogin(){

        hideHUD();

        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_EMAIL, _email);
        intent.putExtra(Constants.KEY_PWD, _password);
        setResult(RESULT_OK, intent);

        finish();
    }

    @OnClick(R.id.imv_edit) void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();

                } else if (item == 1){
                    doTakeGallery();

                } else return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        _photoPath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case Crop.REQUEST_CROP: {

                if (resultCode == RESULT_OK){

                    try {

                        File outFile = BitmapUtils.getOutputMediaFile(this, "temp.jpg");

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                break;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, returnedBitmap.getWidth(), returnedBitmap.getHeight(), true);
                        File newFile = BitmapUtils.getOutputMediaFile(this, System.currentTimeMillis() + ".jpg");
                        BitmapUtils.saveOutput(newFile, w_bmpSizeLimited);
                        _photoPath = newFile.getAbsolutePath();

                        Log.d("photopath===", _photoPath);
                        // imv_avatar(_photoPath);

                        //new feature
                        imv_user.setImageBitmap(w_bmpSizeLimited);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            }

            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();

                    beginCrop(_imageCaptureUri);
                }
                break;

            case Constants.PICK_FROM_CAMERA:
            {
                if (resultCode == RESULT_OK){
                    try {
                        String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                        Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);
                        String w_strFilePath = "";
                        String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                        if (w_strLimitedImageFilePath != null) {
                            w_strFilePath = w_strLimitedImageFilePath;
                        }

                        _photoPath = w_strFilePath;
                        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
                        //  _photoPath= BitmapUtils.getSizeLimitedImageFilePath(_photoPath);
                        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

                        beginCrop(_imageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                break;
            }

            default: break;

        }
    }

    private void beginCrop(Uri source) {

        Uri destination = Uri.fromFile(BitmapUtils.getOutputMediaFile(this, "temp.jpg"));
        Crop.of(source, destination).withMaxSize(840, 1024).start(this);
    }

    @OnClick(R.id.txv_login) void gotoLogin(){

        finish();
    }

    @OnClick(R.id.imv_back) void gotoBack(){

        finish();
    }

    private boolean checkedValid(){

        if (_photoPath.isEmpty()){
            showToast(R.string.add_photo);
            return false;
        }

        else if (_first_name.isEmpty()){
            showToast(R.string.enter_first_name);
            return false;
        }

        else if (_last_name.isEmpty()){
            showToast(R.string.enter_last_name);
            return false;
        }
        else if (_email.isEmpty()){

            showToast(R.string.enter_email);
            return false;
        }
        else if (_phoneNumber.isEmpty()){

            showToast(R.string.enter_phone_number);
            return false;
        }

        else if (_id_number.length() != 8){

            showToast(R.string.enter_id_number);
            return false;
        }
        else if(_password.isEmpty()){

            showToast(R.string.enter_password);
            return false;
        }
        else if (!checkbox.isChecked()){
            showToast(R.string.check_terms);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
