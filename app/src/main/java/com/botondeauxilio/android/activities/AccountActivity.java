package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.BitmapUtils;
import com.botondeauxilio.android.utils.Constants;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pixplicity.easyprefs.library.Prefs;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class AccountActivity extends BaseActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private FirebaseAuth mAuth;
    StorageReference riversRef;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.imv_user) CircleImageView imv_user;
    @BindView(R.id.edt_first_name) EditText edt_first_name;
    @BindView(R.id.edt_last_name) EditText edt_last_name;
    @BindView(R.id.edt_phone) EditText edt_phone;
    @BindView(R.id.edt_id_number) EditText edt_id_number;
    @BindView(R.id.edt_email) EditText edt_email;

    String _email = "", _idNumber = "", _first_name = "", _last_name = "", _phoneNumber = "";
    UserModel ME = g_user;

    Uri _imageCaptureUri;
    String _photoPath = "";

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        loadLayout();
    }

    private void loadLayout(){

        //setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBack();
            }
        });

        edt_first_name.setText(ME.firstName);
        edt_last_name.setText(ME.lastName);
        edt_email.setText(ME.email);
        edt_email.setFocusable(false);
        edt_id_number.setText(ME.idNumber);
        edt_phone.setText(ME.phoneNumber);
        Glide.with(this).load(ME.photoUrl).placeholder(R.drawable.img_user).into(imv_user);

    }

    @OnClick(R.id.btn_update) void update(){

        _first_name = edt_first_name.getText().toString();
        _last_name = edt_last_name.getText().toString();
        _email = edt_email.getText().toString();
        _idNumber = edt_id_number.getText().toString();
        _phoneNumber = edt_phone.getText().toString();

        if (checkedValid()){

            showHUD();
            if (_photoPath.isEmpty()){
                saveAccount(ME.photoUrl);
            }
            else{
                uploadPhoto();
            }

        }

    }

    private void saveAccount(String photoUrl){

        WriteBatch batch = db.batch();
        FirebaseUser user = mAuth.getCurrentUser();

        DocumentReference userRef = db.collection("users").document(user.getUid());

        batch.update(userRef,"first_name", _first_name);
        batch.update(userRef,"last_name", _last_name);
        batch.update(userRef,"phoneNumber", _phoneNumber);
        batch.update(userRef,"email", _email);
        batch.update(userRef,"_idNumber", _idNumber);
        batch.update(userRef,"fcmToken", Prefs.getString(Constants.TOKEN, ""));
        batch.update(userRef, "contactID", g_user.contactID);
        batch.update(userRef,"photoUrl", photoUrl);

        batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                hideHUD();

                g_user = new UserModel(user.getUid(), _first_name, _last_name, _email, _idNumber, _phoneNumber,
                        Prefs.getString(Constants.TOKEN, ""), g_user.contactID, photoUrl);

                showToast(R.string.updated);
                gotoBack();
            }
        });
    }

    private void uploadPhoto(){

        Uri file = Uri.fromFile(new File(_photoPath));
        riversRef = storage.getReference().child("profile/" + g_user.id);
        UploadTask uploadTask = riversRef.putFile(file);
        Log.d("USER ID", g_user.id);

        // Register observers to listen for when the download is done or if it fails
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    hideHUD();
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return riversRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    saveAccount(downloadUri.toString());
                } else {
                    hideHUD();
                    showToast(getString(R.string.photo_failed));
                }
            }
        });

    }

    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }


    @OnClick(R.id.imv_edit) void editPhoto() {

        if (hasPermissions(this, PERMISSIONS)){

            selectPhoto();
        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }

    private void selectPhoto(){

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();

                } else if (item == 1){
                    doTakeGallery();

                } else return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        _photoPath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case Crop.REQUEST_CROP: {

                if (resultCode == RESULT_OK){

                    try {

                        File outFile = BitmapUtils.getOutputMediaFile(this, "temp.png");

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                break;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, returnedBitmap.getWidth(), returnedBitmap.getHeight(), true);
                        File newFile = BitmapUtils.getOutputMediaFile(this, System.currentTimeMillis() + ".jpg");
                        BitmapUtils.saveOutput(newFile, w_bmpSizeLimited);
                        _photoPath = newFile.getAbsolutePath();

                        Log.d("photopath===", _photoPath);
                        // imv_avatar(_photoPath);

                        //new feature
                        imv_user.setImageBitmap(w_bmpSizeLimited);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            }

            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();

                    beginCrop(_imageCaptureUri);
                }
                break;

            case Constants.PICK_FROM_CAMERA:
            {
                if (resultCode == RESULT_OK){
                    try {
                        String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                        Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);
                        String w_strFilePath = "";
                        String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                        if (w_strLimitedImageFilePath != null) {
                            w_strFilePath = w_strLimitedImageFilePath;
                        }

                        _photoPath = w_strFilePath;
                        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
                        //  _photoPath= BitmapUtils.getSizeLimitedImageFilePath(_photoPath);
                        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

                        beginCrop(_imageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                break;
            }

            default: break;

        }
    }

    private void beginCrop(Uri source) {

        Uri destination = Uri.fromFile(BitmapUtils.getOutputMediaFile(this, "temp.png"));
        Crop.of(source, destination).withMaxSize(840, 1024).start(this);
    }

    private boolean checkedValid(){

        if (_photoPath.isEmpty() && ME.photoUrl.isEmpty()){
            showToast(R.string.add_photo);
            return false;
        }
        else if (_first_name.isEmpty()){
            showToast(R.string.enter_first_name);
            return false;
        }
        else if (_last_name.isEmpty()){
            showToast(R.string.enter_first_name);
            return false;
        }
        else if (_email.isEmpty()){
            showToast(R.string.enter_email);
            return false;
        }
        else if (_phoneNumber.isEmpty()){
            showToast(R.string.enter_phone_number);
        }
        else if (_idNumber.isEmpty()){
            showToast(R.string.enter_id_number);
            return false;
        }

        return true;
    }

    private void gotoBack(){

        finish();
    }
    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
