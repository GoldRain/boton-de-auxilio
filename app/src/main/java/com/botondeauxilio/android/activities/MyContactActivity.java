package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.adapter.MyContactListAdapter;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;


public class MyContactActivity extends BaseActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth;
    FirebaseUser user;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.lst_send) ListView lst_send;
    MyContactListAdapter adapter;
    ArrayList<UserModel> contacts/* = new ArrayList<>()*/;
    ArrayList<UserModel> temContact = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contact);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBack();
            }
        });

        adapter = new MyContactListAdapter(this);
        lst_send.setAdapter(adapter);

        getContact();
    }

    private void getContact(){

        showHUD();

        db.collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                hideHUD();

                if (task.isSuccessful()) {

                    contacts = new ArrayList<>();

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        for (int i = 0; i < g_user.contactID.size(); i++){

                            if (g_user.contactID.get(i).equals(document.getId())){

                                Log.d("TAG", "DocumentSnapshot data: " + document.getData());

                                String fcmToken = "";
                                if (document.getData().containsKey("fcmToken")) {
                                    fcmToken = document.getData().get("fcmToken").toString();
                                }

                                ArrayList<String> contactID = new ArrayList<>();
                                if (document.getData().containsKey("contactID")){
                                    contactID = (ArrayList<String>)document.getData().get("contactID");
                                }

                                UserModel userModel = new UserModel(
                                        document.getId(),
                                        document.getData().get("first_name").toString(),
                                        document.getData().get("last_name").toString(),
                                        document.getData().get("email").toString(),
                                        document.getData().get("idNumber").toString(),
                                        document.getData().get("phoneNumber").toString(),
                                        fcmToken,
                                        contactID,
                                        document.getData().get("photoUrl").toString());

                                contacts.add(userModel);
                                break;
                            }
                        }
                        adapter.setList(contacts);
                    }

                } else {
                    Log.d("MyContact", "get failed with ", task.getException());
                }
            }
        });
    }


    @OnClick(R.id.txv_add) void addContact(){

        Intent intent = new Intent(this,ContactManageActivity.class);
        startActivityForResult(intent, Constants.KEY_CONTACT_CODE);

        contacts.removeAll(temContact);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.KEY_CONTACT_CODE && resultCode == RESULT_OK && data != null){

            ArrayList<UserModel> newUser = (ArrayList<UserModel>) data.getSerializableExtra(Constants.KEY_CONTACTS);
            
            temContact.clear();
            temContact = newUser;
            contacts.addAll(temContact);

            if (temContact != null)
                adapter.setList(contacts);
        }
    }

    public void deleteContact(UserModel contact){

        contacts.remove(contact);
        adapter.setList(contacts);
    }

    @OnClick(R.id.btn_save) void setContact(){

        if (!contacts.isEmpty()){

            ArrayList<String> contactID = new ArrayList<>();

            for (int i = 0; i < contacts.size(); i++){
                contactID.add(contacts.get(i).id);
            }

            saveContact(contactID);
        }
        else showToast(R.string.add_contact);

    }

    private void saveContact(ArrayList<String> contactID){

        showHUD();

        WriteBatch batch = db.batch();

        DocumentReference userRef = db.collection("users").document(g_user.id);

        batch.update(userRef, "contactID", contactID);

        batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                temContact.clear();

                hideHUD();

                g_user.contactID = contactID;

                showToast(R.string.saved);

            }
        });

    }


    private void gotoBack(){

        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
