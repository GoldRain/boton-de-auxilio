package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pixplicity.easyprefs.library.Prefs;

import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

import static com.botondeauxilio.android.BotondeAuxilioApplication.printKeyHash;

public class SplashActivity extends BaseActivity {

    LocationManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //printKeyHash(this);
        //loadLayout();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {

                String newToken = instanceIdResult.getToken();
                Log.d("S_NEW_TOKEN", newToken);
                Prefs.putString(Constants.TOKEN, newToken);
            }
        });
    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.ACCESS_COARSE_LOCATION, PermissionEnum.ACCESS_FINE_LOCATION)
                .key(105)
                .askAgain(true)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){

                        if (allPermissionsGranted)
                            statusCheck();
                    }
                })
                .ask(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkPermission();
                /*startActivity(new Intent(SplashActivity.this, LogInActivity.class));
                finish();*/
            }
        }, 500);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 105 && grantResults[0] == 0){

            statusCheck();
        }
        else finish();
    }

    public void statusCheck() {

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }else {
            startActivity(new Intent(SplashActivity.this, LogInActivity.class));
            finish();
        }

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
