package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.utils.Constants;
import com.botondeauxilio.android.utils.TouchImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImagePreviewActivity extends BaseActivity {

    @BindView(R.id.imv_image) ImageView imv_image;
    String _imagePath = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        ButterKnife.bind(this);
        _imagePath = (String) getIntent().getStringExtra(Constants.KEY_IMAGE);
        loadLayout();
    }

    private void loadLayout(){


        Glide.with(this).load(_imagePath).into(imv_image);

    }

    @OnClick(R.id.imv_back) void gotoFinish(){

        finish();
    }

    @Override
    public void onBackPressed() {
        gotoFinish();
    }
}