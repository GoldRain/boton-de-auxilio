package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.utils.BitmapUtils;
import com.botondeauxilio.android.utils.Constants;
import com.botondeauxilio.android.utils.GPSTracker;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

import static com.botondeauxilio.android.utils.Constants.LATITUDE;
import static com.botondeauxilio.android.utils.Constants.LONGITUDE;
import static com.botondeauxilio.android.utils.Constants.g_user;

public class MainActivity extends BaseActivity {

    FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.imv_user) CircleImageView imv_user;

    Uri _imageCaptureUri;
    String _photoPath = "";
    double latitude = 0;
    double longitude = 0;
    boolean fromBlue = false;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    boolean _takenPhoto = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        loadLayout();
        updateToken();
        getGPS();

    }

    private void getGPS(){

        // check if GPS enabled
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled())
        {

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            LATITUDE = latitude;
            LONGITUDE = longitude;
        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }

    private void loadLayout(){

        Glide.with(this).load(g_user.photoUrl).placeholder(R.drawable.img_user).into(imv_user);

    }

    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    @OnClick(R.id.imv_send_blue) void sendBlueWithoutPhoto(){
        fromBlue = true;
        _takenPhoto = false;
        sendNotification(Constants.g_user.getName(), Constants.g_user.getName() + " " + getString(R.string.body_blue), "");

    }

    private void sendBlueWithPhoto(){

        Log.d("sendB Photo", "yes");

        if (hasPermissions(this, PERMISSIONS)){

            if (g_user.contactID.size() > 0){

                fromBlue = true;
                doTakePhoto();
            }
            else showToast(getString(R.string.no_contact));

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }



    @OnClick(R.id.imv_send_red) void sendRedWithoutPhoto(){

        fromBlue = false;
        _takenPhoto = false;
        sendNotification(Constants.g_user.getName(), Constants.g_user.getName() + " " + getString(R.string.body_red), "");

    }

    private void sendRedWithPhoto(){


        if (hasPermissions(this, PERMISSIONS)){

            if (g_user.contactID.size() > 0){
                fromBlue = false;
                doTakePhoto();
            }
            else showToast(getString(R.string.no_contact));

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    @OnClick(R.id.txv_tutorial) void playTutorial(){

        startActivity(new Intent(this, VideoViewActivity.class));
    }

    private /*Task<String>*/void sendNotification(String title, String body, String photoUrl) {

        showHUD();

        ArrayList<String> receiverID = Constants.g_user.contactID;

        for (int i = 0; i < Constants.g_user.contactID.size(); i++){

            Log.d("IDS==>", Constants.g_user.contactID.get(i));
            Log.d("receiverID==>", receiverID.get(i));
        }
        // Create the arguments to the callable function.
        Map<String, Object> data = new HashMap<>();
        data.put("title", title);
        data.put("body", body);
        data.put("receiverID", receiverID);
        data.put("senderID", Constants.g_user.id);
        data.put("latitude", String.valueOf(latitude));
        data.put("longitude", String.valueOf(longitude));
        data.put("photoUrl", photoUrl);
        data.put("sound","noti_sound.mp3");
        data.put("type", "notification");
        data.put("status", "normal");

        /*return */mFunctions
                .getHttpsCallable("sendNotification")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        hideHUD();

                        Log.d("status", String.valueOf(_takenPhoto));

                        if (!_takenPhoto){

                            Log.d("sendBlue", String.valueOf(fromBlue));
                            if (fromBlue){
                                Log.d("sentTake", String.valueOf(fromBlue));
                                sendBlueWithPhoto();
                            }
                            else sendRedWithPhoto();
                        }

                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        return result;
                    }
                });

    }

    @OnClick(R.id.btn_config) void gotoSendNotification(){

        Intent intent = new Intent(this, MyContactActivity.class);
        startActivity(intent);
        //finish();
    }

    @OnClick(R.id.ryt_notification) void gotoAllNotification(){

        Intent intent = new Intent(this, NotificationHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_account) void gotoAccount(){

        Intent intent = new Intent(this, AccountActivity.class);
        startActivity(intent);
    }

    private void updateToken(){

        if (g_user.token.isEmpty()){

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {

                    String newToken = instanceIdResult.getToken();

                    Prefs.putString(Constants.TOKEN, newToken);

                    db.collection("users").document(g_user.id)

                            .update("fcmToken", Prefs.getString(Constants.TOKEN,""))
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override public void onSuccess(Void aVoid) {
                                    g_user.token = newToken;
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                }
                        });

                }
            });

        }
    }

    private void uploadPhoto(String title, String body){

        showHUD();
        Uri file = Uri.fromFile(new File(_photoPath));

        Log.d("photoPath", _photoPath);

        StorageReference riversRef = storage.getReference().child("notification/" + file.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(file);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    hideHUD();
                    showToast(getString(R.string.upload_failed));
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return riversRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    hideHUD();

                    Uri photoUrl = task.getResult();
                    sendNotification(title, body, photoUrl.toString());

                } else {

                    hideHUD();
                    showToast(getString(R.string.upload_failed));
                }
            }
        });
    }

    private void doTakePhoto(){

        Log.d("Photo", "yes");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        _photoPath = BitmapUtils.getUploadFolderPath() + System.currentTimeMillis() + ".jpg";

        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        {
            if (resultCode == RESULT_OK) {
                try {

                    String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                    Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);
                    String w_strFilePath = "";
                    String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                    if (w_strLimitedImageFilePath != null) {
                        w_strFilePath = w_strLimitedImageFilePath;
                    }

                    _photoPath = w_strFilePath;

                    Log.d("photoPath==>", _photoPath);

                   /* if (g_user.token.isEmpty()) {
                        updateToken();
                        return;
                    }
                    if (checkedValid())
                        uploadPhoto(Constants.g_user.getName(), Constants.g_user.getName() + " " + getString(R.string.body_blue));*/

                   _takenPhoto = true;

                    if (!_photoPath.isEmpty()){

                        if (fromBlue){

                            if (g_user.token.isEmpty()) {
                                updateToken();
                                return;
                            }
                            if (checkedValid())
                                uploadPhoto(Constants.g_user.getName(), Constants.g_user.getName() + " " + getString(R.string.body_blue));
                        }
                        else {

                            if (g_user.token.isEmpty()) {
                                updateToken();
                                return;
                            }
                            if (checkedValid())
                                uploadPhoto(Constants.g_user.getName(), Constants.g_user.getName() + " " + getString(R.string.body_red));
                        }
                    }
                    else {
                        showToast(getString(R.string.send_notification_failed));
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnClick(R.id.txv_logout) void logOut(){

        Prefs.putString(Constants.PREFKEY_USEREMAIL, "");
        Prefs.putString(Constants.PREFKEY_USERPWD, "");
        Prefs.putString(Constants.TOKEN, "");

        Intent intent = new Intent(this, LogInActivity.class);
        intent.putExtra(Constants.KEY_LOGOUT, true);
        startActivity(intent);
    }

    private boolean checkedValid(){

        if (Constants.g_user.contactID.isEmpty()){
            showToast(R.string.add_contact);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateToken();
        getGPS();
    }

}
