package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.adapter.NotificationHistoryListAdapter;
import com.botondeauxilio.android.models.NotificationModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;
import static com.google.firebase.firestore.Query.Direction.DESCENDING;

public class NotificationHistoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.lst_notification) ListView lst_notification;
    @BindView(R.id.swipe_layout) SwipeRefreshLayout swipe_layout;

    NotificationHistoryListAdapter adapter;
    ArrayList<NotificationModel> allNotifications = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_history);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout(){

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoBack();
            }
        });


        swipe_layout.setOnRefreshListener(this);
        adapter = new NotificationHistoryListAdapter(this);
        lst_notification.setAdapter(adapter);

        swipe_layout.post(new Runnable() {
            @Override
            public void run() {
                getAllHistory();
            }
        });
    }

    private void getAllHistory(){

        allNotifications = new ArrayList<>();

        swipe_layout.setRefreshing(true);

        db.collection("notifications").orderBy("timestamp", DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        swipe_layout.setRefreshing(false);

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d("NotificationActivity", document.getId() + " => " + document.getData());

                                if (!g_user.id.equals(document.getData().get("receiverID"))){
                                    continue;
                                }

                                String type = document.get("type").toString();

                                String status = "";
                                if (document.getData().containsKey("status")){
                                    status = document.get("status").toString();
                                }


                                String latitude = "";
                                if (document.getData().containsKey("latitude")) {
                                    latitude = document.getData().get("latitude").toString();
                                }

                                String longitude = "";
                                if (document.getData().containsKey("longitude")) {
                                    longitude = document.getData().get("longitude").toString();
                                }

                                String photoUrl = "";
                                if (document.getData().containsKey("photoUrl")) {
                                    photoUrl = document.getData().get("photoUrl").toString();
                                }

                                if (type.equalsIgnoreCase("notification")){

                                    NotificationModel notification = new NotificationModel(
                                            document.getId(),
                                            document.getData().get("title").toString(),
                                            document.getData().get("body").toString(),
                                            document.getData().get("receiverID").toString(),
                                            document.getData().get("senderID").toString(),
                                            document.getData().get("timestamp").toString(),
                                            latitude,
                                            longitude,
                                            photoUrl,
                                            type,
                                            status);

                                    allNotifications.add(notification);
                                }
                                else if (type.equalsIgnoreCase("reply")){
                                    NotificationModel notification = new NotificationModel(
                                            document.getId(),
                                            document.getData().get("title").toString(),
                                            document.getData().get("body").toString(),
                                            document.getData().get("receiverID").toString(),
                                            document.getData().get("senderID").toString(),
                                            document.getData().get("timestamp").toString(),
                                            type,
                                            status);

                                    allNotifications.add(notification);
                                }

                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    adapter.setList(allNotifications);
                                }
                            });

                        } else {
                            Log.d("NotiActivity", "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    @OnClick(R.id.txv_delete_all) void deleteAllNoti(){

        if (allNotifications.size() == 0)
            return;

        showHUD();

        for (int i = 0; i < allNotifications.size(); i++){

            db.collection("notifications").document(allNotifications.get(i).id)
                    .delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            hideHUD();

                            allNotifications.clear();
                            adapter.setList(allNotifications);
                            Log.d("Notification", "All successfully deleted!");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("Notification", "Error deleting document", e);
                        }
                    });


            //Delete notification image
            // Create a storage reference from our app
            StorageReference storageRef = storage.getReference();
            StorageReference desertRef = storageRef.child("notification/" + allNotifications.get(i).photoUrl);
            // Delete the file
            desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("Notification Image", "All successfully deleted!");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Uh-oh, an error occurred!
                }
            });
        }

    }

    public void deleteNoti(NotificationModel noti){

        showHUD();

        db.collection("notifications").document(noti.id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        hideHUD();

                        allNotifications.remove(noti);
                        adapter.setList(allNotifications);
                        Log.d("Notification", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Notification", "Error deleting document", e);
                    }
                });

        //Delete notification image
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        StorageReference desertRef = storageRef.child("notification/" + noti.photoUrl);
        // Delete the file
        desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Notification Image", "All successfully deleted!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });

    }

    public void showMap(double latitude, double longitude){

        String uri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude;
        //String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    @Override
    public void onRefresh() {

        getAllHistory();
    }

    private void gotoBack(){

        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
