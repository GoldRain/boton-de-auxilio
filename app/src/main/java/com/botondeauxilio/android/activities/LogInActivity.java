package com.botondeauxilio.android.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.models.RequestModel;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class LogInActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_password) EditText edt_password;
    String _email = "", _password = "";
    boolean _isFromLogout = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);
        initValue();
        loadLayout();
    }

    private void initValue(){

        _isFromLogout = getIntent().getBooleanExtra(Constants.KEY_LOGOUT, false);
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            Prefs.putString(Constants.PREFKEY_USEREMAIL,"");
            Prefs.putString(Constants.PREFKEY_USERPWD, "");

            edt_email.setText("");
            edt_password.setText("");

        } else {

            String email = Prefs.getString( Constants.PREFKEY_USEREMAIL, "");
            String password = Prefs.getString(Constants.PREFKEY_USERPWD, "");

            edt_email.setText(email);
            edt_password.setText(password);

            if ( email.length()>0 && password.length() > 0 ) {

                _email = edt_email.getText().toString();
                _password = edt_password.getText().toString();

                userLogin();
            }
        }
    }

    @OnClick(R.id.btn_login) void gotoLogin() {

        _email = edt_email.getText().toString();
        _password = edt_password.getText().toString();

        if (checkedValid())
            userLogin();
    }

    private void gotoMain(){

        hideHUD();

        startActivity(new Intent(LogInActivity.this, MainActivity.class));
        finish();
    }

    private void userLogin() {

        showHUD();

        mAuth.signInWithEmailAndPassword(_email, _password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();

                            assert user != null;
                            Log.d("verified==>", String.valueOf(user.isEmailVerified()));
                            if (user.isEmailVerified()){

                                ArrayList<RequestModel> requestID = new ArrayList<>();

                                db.collection("users").document(user.getUid()).collection("requests").get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                if (task.isSuccessful()){

                                                    for (QueryDocumentSnapshot document : task.getResult()){

                                                        Boolean status = (Boolean)document.get("status");

                                                        RequestModel request = new RequestModel(
                                                                document.getId().toString(),
                                                                document.get("receiverID").toString(),
                                                                status,
                                                                document.get("timeStamp").toString());


                                                        requestID.add(request);

                                                    }

                                                    db.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                            if (task.isSuccessful()) {

                                                                DocumentSnapshot document = task.getResult();

                                                                if (document.exists()) {

                                                                    Log.d("LogInActivity", document.getData() + user.getUid());


                                                                    String fcmToken = "";
                                                                /*if (document.getData().containsKey("fcmToken")) {
                                                                    fcmToken = document.getData().get("fcmToken").toString();
                                                                }
*/

                                                                    ArrayList<String> contactID = new ArrayList<>();
                                                                    if (document.getData().containsKey("contactID")){
                                                                        contactID = (ArrayList<String>)document.getData().get("contactID");
                                                                    }

                                                                    ArrayList<RequestModel> receivedID = new ArrayList<>();

                                                                    /*if (document.getData().containsKey("receivedID")){
                                                                        receivedID = (ArrayList<RequestModel>) document.getData().get("receivedID");
                                                                    }*/

                                                                    List<Map<String, Object>> maps = new ArrayList<>();
                                                                    if (document.getData().containsKey("receivedID")){
                                                                        maps = (List<Map<String, Object>>) document.get("receivedID");
                                                                    }

                                                                    for (int i = 0; i < maps.size(); i++){

                                                                        RequestModel model = new RequestModel();
                                                                        model.receiverID = maps.get(i).get("receiverID").toString();
                                                                        model.timeStamp = maps.get(i).get("timeStamp").toString();

                                                                        receivedID.add(model);
                                                                    }


                                                                    db.collection("users").document(user.getUid())

                                                                            .update("fcmToken", Prefs.getString(Constants.TOKEN, ""))
                                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                @Override
                                                                                public void onSuccess(Void aVoid) {
                                                                                    //fcmToken = Prefs.getString(Constants.TOKEN, "");
                                                                                    g_user.token = Prefs.getString(Constants.TOKEN, "");
                                                                                }
                                                                            })
                                                                            .addOnFailureListener(new OnFailureListener() {
                                                                                @Override
                                                                                public void onFailure(@NonNull Exception e) {
                                                                                }
                                                                            });


                                                                    UserModel userModel = new UserModel(
                                                                            user.getUid(),
                                                                            document.getData().get("first_name").toString(),
                                                                            document.getData().get("last_name").toString(),
                                                                            document.getData().get("email").toString(),
                                                                            document.getData().get("idNumber").toString(),
                                                                            document.getData().get("phoneNumber").toString(),
                                                                            g_user.token,
                                                                            contactID,
                                                                            document.getData().get("photoUrl").toString(),
                                                                            receivedID,
                                                                            requestID);

                                                                    g_user = userModel;


                                                                    Prefs.putString(Constants.PREFKEY_USEREMAIL, _email);
                                                                    Prefs.putString(Constants.PREFKEY_USERPWD, _password);

                                                                    gotoMain();

                                                                } else {
                                                                    Log.d("LogInActivity", "No such document");
                                                                }
                                                            } else {
                                                                Log.d("LogInActivity", "get failed with ", task.getException());
                                                            }
                                                        }
                                                    });
                                                }
                                                else showToast(getString(R.string.error_document));
                                            }
                                        });

                            }
                            else {

                                hideHUD();
                                user.sendEmailVerification()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                showToast("Please verify with your email");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                showToast("Please verify with your email");
                                            }
                                        });
                            }

                        } /*else {

                            hideHUD();

                            // If sign in fails, display a message to the user.
                            Log.w("LogInActivity", "signInWithEmail:failure", task.getException());
                            showToast("Invalid user email and password");
                        }*/
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideHUD();
                        showToast(e.getMessage());
                    }
                });
    }

    @OnClick(R.id.txv_signup) void gotoSign(){

        Intent intent = new Intent(this, SignUpActivity.class);
        startActivityForResult(intent, Constants.KEY_SIGNUP_CODE);
    }

    @OnClick(R.id.txv_forgot) void forgotPassword(){

        _email = edt_email.getText().toString();
        if (_email.isEmpty()){
            showToast(R.string.enter_email);
            return;
        }
        FirebaseAuth.getInstance().sendPasswordResetEmail(_email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            showToast("Password reset email sent");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {

                        showToast(e.getMessage());
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.KEY_SIGNUP_CODE && resultCode == RESULT_OK){

            String email = data.getStringExtra(Constants.KEY_EMAIL);
            String pwd = data.getStringExtra(Constants.KEY_PWD);

            Log.d("from_sign up", email + ":" + pwd);

            edt_email.setText(email);
            edt_password.setText(pwd);

            gotoLogin();
        }
    }

    private boolean checkedValid(){

        if (_email.isEmpty()){

            showToast(R.string.enter_email);
            return false;
        }
        else if(_password.isEmpty()){

            showToast(R.string.enter_password);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
