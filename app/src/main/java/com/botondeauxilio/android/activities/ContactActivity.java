package com.botondeauxilio.android.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.adapter.ContactListAdapter;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class ContactActivity extends BaseActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @BindView(R.id.lst_contact) ListView lst_contact;
    ContactListAdapter adapter;
    ArrayList<UserModel> contacts = new ArrayList<>();
    public ArrayList<UserModel> checkedUser = new ArrayList<>();

    @BindView(R.id.imv_search) ImageView imv_searchIcon;
    @BindView(R.id.edt_search) EditText edt_search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);
        loadLayout();
    }

    public void refreshLayout(){

        lst_contact.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        //adapter = new ContactListAdapter(this);
        lst_contact.setAdapter(adapter);
        adapter.setList(contacts);
    }

    private void loadLayout(){

        //lst_contact.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        //adapter = new ContactListAdapter(this);
        lst_contact.setAdapter(adapter);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0){
                    imv_searchIcon.setImageResource(R.drawable.ic_cancel);
                }else imv_searchIcon.setImageResource(R.drawable.ic_search);
            }

            @Override
            public void afterTextChanged(Editable s) {

                String str_search = edt_search.getText().toString();
                //adapter.searchContact(str_search);
            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        imv_searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_search.setText("");
            }
        });

        getAllContact();
    }

    private void getAllContact(){

        //showHUD();

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        //hideHUD();
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if (Constants.g_user.id.equals(document.getId()))
                                    continue;

                                boolean is_exist =  false;

                                for (int i = 0; i < g_user.contactID.size(); i++){

                                    if (g_user.contactID.get(i).equals(document.getId())){
                                        is_exist = true;
                                        break;
                                    }

                                }

                                if (!is_exist){

                                    String fcmToken = "";
                                    if (document.getData().containsKey("fcmToken")) {
                                        fcmToken = document.getData().get("fcmToken").toString();
                                    }

                                    ArrayList<String> contactID = new ArrayList<>();
                                    if (document.getData().containsKey("contactID")){
                                        contactID = (ArrayList<String>)document.getData().get("contactID");
                                    }

                                    UserModel userModel = new UserModel(
                                            document.getId(),
                                            document.getData().get("first_name").toString(),
                                            document.getData().get("last_name").toString(),
                                            document.getData().get("email").toString(),
                                            document.getData().get("idNumber").toString(),
                                            document.getData().get("phoneNumber").toString(),
                                            fcmToken,
                                            contactID,
                                            document.getData().get("photoUrl").toString());

                                    contacts.add(userModel);
                                }
                            }

                            adapter.setList(contacts);

                        } else {
                            Log.d("ContactActivity", "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    @OnClick(R.id.btn_add) void addContacts(){

        gotoSend(adapter.getCheckedContact());

        /*ArrayList<UserModel> result = new ArrayList<>();
        SparseBooleanArray checkedItems = lst_contact.getCheckedItemPositions();

        for (int i = 0; i < checkedItems.size(); i++){
            Log.d("checkedItem==>", String.valueOf(checkedItems.keyAt(i)));
            result.add(checkedUser.get(checkedItems.keyAt(i)));
            Log.d("friends==>", result.get(i).getName());
        }

        if (result.size() > 0)
            gotoSend(result);
        else
            showToast("Add contact");*/

    }

    private void gotoSend(ArrayList<UserModel> sender){

        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_CONTACTS, sender);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
