package com.botondeauxilio.android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.MyContactActivity;
import com.botondeauxilio.android.models.UserModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyContactListAdapter extends BaseAdapter {

    ArrayList<UserModel> contacts = new ArrayList<>();
    MyContactActivity activity;
    LayoutInflater inflater;

    public MyContactListAdapter(MyContactActivity activity){

        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setList(ArrayList<UserModel> userModels){

        contacts = userModels;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){

            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_add_list, parent, false);

            viewHolder.imv_user = (CircleImageView)convertView.findViewById(R.id.imv_user);
            viewHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            viewHolder.txv_id_number = (TextView)convertView.findViewById(R.id.item_id_number);
            viewHolder.imb_delete = (ImageButton)convertView.findViewById(R.id.imb_delete);

            convertView.setTag(viewHolder);
        }
        else{

            viewHolder = (ViewHolder)convertView.getTag();
        }

        UserModel contact = (UserModel) contacts.get(position);

        Glide.with(activity).load(contact.photoUrl).into(viewHolder.imv_user);
        viewHolder.txv_name.setText(contact.getName());
        viewHolder.txv_id_number.setText(contact.idNumber);

        viewHolder.imb_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.deleteContact(contact);
            }
        });

        return convertView;
    }

    public class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_id_number;
        ImageButton imb_delete;
    }
}
