package com.botondeauxilio.android.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.ContactManageActivity;
import com.botondeauxilio.android.fragment.ContactsFragment;
import com.botondeauxilio.android.fragment.RequestsFragment;
import com.botondeauxilio.android.fragment.AddContactFragment;

public class ContactManageViewPagerAdapter extends FragmentPagerAdapter {

    ContactManageActivity activity;
    public ContactManageViewPagerAdapter(@NonNull FragmentManager fm, ContactManageActivity activity) {
        super(fm);
        this.activity = activity;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        if (position == 0)
        {
            fragment = new ContactsFragment(activity);
        }
        else if (position == 1)
        {
            fragment = new RequestsFragment(activity);
        }
        else if (position == 2)
        {
            fragment = new AddContactFragment(activity);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";

        switch (position){
            case 0:
                title = activity.getString(R.string.contacts);
                break;
            case 1:
                title = activity.getString(R.string.requests);
                break;
            case 2:
                title = activity.getString(R.string.add_contact_);
                break;
        }
        return title;
    }

}
