package com.botondeauxilio.android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.fragment.RequestsFragment;
import com.botondeauxilio.android.models.UserModel;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class AcceptRequestListAdapter extends BaseAdapter {

    Context context;
    RequestsFragment fragment;
    ArrayList<UserModel> requests = new ArrayList<>();
    LayoutInflater inflater;

    public AcceptRequestListAdapter(Context context, RequestsFragment fragment){

        this.context = context;
        this.fragment = fragment;
        this.inflater = LayoutInflater.from(context);

    }

    public AcceptRequestListAdapter(Context context){

        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }


    public void setList(ArrayList<UserModel> allRequests){
        this.requests = allRequests;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int position) {
        return requests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null){

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_accept_request_list, parent, false);

            holder.imv_user = (CircleImageView)convertView.findViewById(R.id.imv_user);
            holder.txv_name = (TextView) convertView.findViewById(R.id.txv_name);
            holder.txv_date = (TextView) convertView.findViewById(R.id.txv_date);
            holder.btn_accept = (Button) convertView.findViewById(R.id.btn_accept);
            holder.btn_reject = (Button) convertView.findViewById(R.id.btn_reject);

            convertView.setTag(holder);
        }
        else {

            holder = (ViewHolder)convertView.getTag();
        }

        UserModel request = requests.get(position);

        Glide.with(context).load(request.photoUrl).into(holder.imv_user);
        holder.txv_name.setText(request.getName());
        holder.txv_date.setText(getLocalTime(request.timeStamp));

        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.acceptRequest(request);
            }
        });

        holder.btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.rejectRequest(request);
            }
        });


        return convertView;
    }

    private String getLocalTime(String timestamp){

        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = calendar.getTimeZone();

            int tzt = tz.getOffset(System.currentTimeMillis());
            Log.d("timezone", String.valueOf(tzt));
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
            Date currenTimeZone = (new Date(Long.parseLong(timestamp)));
            return sdf.format(currenTimeZone);

        }catch (Exception e) {
        }
        return "";
    }

    public class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_date;
        Button btn_accept, btn_reject;

    }
}
