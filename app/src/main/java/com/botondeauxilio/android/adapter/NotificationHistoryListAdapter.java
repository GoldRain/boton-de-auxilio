package com.botondeauxilio.android.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.ImagePreviewActivity;
import com.botondeauxilio.android.activities.NotificationHistoryActivity;
import com.botondeauxilio.android.models.NotificationModel;
import com.botondeauxilio.android.utils.Constants;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.botondeauxilio.android.utils.Constants.g_user;

public class NotificationHistoryListAdapter extends BaseAdapter {

    ArrayList<NotificationModel> allNoti = new ArrayList<>();
    NotificationHistoryActivity activity;
    LayoutInflater inflater;

    FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public NotificationHistoryListAdapter(NotificationHistoryActivity activity){

        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setList(ArrayList<NotificationModel> noti){

        allNoti = noti;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allNoti.size();
    }

    @Override
    public Object getItem(int position) {
        return allNoti.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){

            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_noti_history_list, parent, false);
            viewHolder.imv_sender = (CircleImageView)convertView.findViewById(R.id.imv_user);
            viewHolder.txv_title = (TextView)convertView.findViewById(R.id.txv_title);
            viewHolder.txv_content = (TextView)convertView.findViewById(R.id.txv_content);
            viewHolder.txv_date  = (TextView)convertView.findViewById(R.id.txv_date);
            viewHolder.imb_delete = (ImageButton)convertView.findViewById(R.id.imb_delete);
            viewHolder.imv_photo = (ImageView)convertView.findViewById(R.id.imv_photo);
            viewHolder.txv_address = (TextView)convertView.findViewById(R.id.txv_address);
            viewHolder.imv_map = (ImageView)convertView.findViewById(R.id.imv_map);
            viewHolder.btn_accept = (Button) convertView.findViewById(R.id.btn_accept);
            viewHolder.btn_reject = (Button) convertView.findViewById(R.id.btn_reject);
            viewHolder.ryt_image = (RelativeLayout) convertView.findViewById(R.id.ryt_image);
            viewHolder.lyt_btn = (LinearLayout) convertView.findViewById(R.id.lyt_btn);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        NotificationModel noti = (NotificationModel)allNoti.get(position);

        if (!noti.type.equalsIgnoreCase("notification")){

            viewHolder.imv_map.setVisibility(View.GONE);
            viewHolder.imv_photo.setVisibility(View.GONE);
            viewHolder.txv_address.setVisibility(View.GONE);
            //viewHolder.btn_accept.setVisibility(View.GONE);
            //viewHolder.btn_reject.setVisibility(View.GONE);
            viewHolder.lyt_btn.setVisibility(View.GONE);
            viewHolder.ryt_image.setVisibility(View.GONE);

        }else {

            if (noti.status.equalsIgnoreCase("sent")) {

                viewHolder.lyt_btn.setVisibility(View.GONE);
            }
            else viewHolder.lyt_btn.setVisibility(View.VISIBLE);

            viewHolder.imv_map.setVisibility(View.VISIBLE);
            viewHolder.imv_photo.setVisibility(View.VISIBLE);
            viewHolder.txv_address.setVisibility(View.VISIBLE);
            //viewHolder.btn_accept.setVisibility(View.VISIBLE);
            //viewHolder.btn_reject.setVisibility(View.VISIBLE);
            viewHolder.ryt_image.setVisibility(View.VISIBLE);

        }

        DocumentReference docRef = db.collection("users").document(noti.senderID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Glide.with(activity).load(document.getData().get("photoUrl").toString()).into(viewHolder.imv_sender);
                    } else {
                        Log.d("Log", "No such document");
                    }
                } else {
                    Log.d("Log", "get failed with ", task.getException());
                }
            }
        });
        
        Glide.with(activity).load(noti.photoUrl).placeholder(R.drawable.bg_background).into(viewHolder.imv_photo);

        viewHolder.txv_title.setText(noti.title);
        viewHolder.txv_content.setText(noti.content);
        viewHolder.txv_date.setText(getLocalTime(noti.date));
        viewHolder.txv_address.setText(noti.getAddress());

        viewHolder.imv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showMap(noti.latitude, noti.longitude);
            }
        });

        viewHolder.imb_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.deleteNoti(noti);
            }
        });

        viewHolder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendAddNoti(noti, activity.getString(R.string.noti_accept), viewHolder.lyt_btn);
            }
        });

        viewHolder.btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendAddNoti(noti, activity.getString(R.string.noti_reject), viewHolder.lyt_btn);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        viewHolder.imv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ImagePreviewActivity.class);
                intent.putExtra(Constants.KEY_IMAGE, noti.photoUrl);
                activity.startActivity(intent);
            }
        });

        return convertView;
    }

    private String getLocalTime(String timestamp){

        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = calendar.getTimeZone();

            int tzt = tz.getOffset(System.currentTimeMillis());
            Log.d("timezone", String.valueOf(tzt));
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM HH:mm");
            Date currenTimeZone = (new Date(Long.parseLong(timestamp)));
            return sdf.format(currenTimeZone);

        }catch (Exception e) {
        }
        return "";
    }

    private Task<String> sendAddNoti(NotificationModel noti , String body, LinearLayout ly){

        // Create the arguments to the callable function.
        activity.showHUD();

        ArrayList<String> receiverIDs = new ArrayList<>();
        receiverIDs.add(noti.senderID);

        Map<String, Object> data = new HashMap<>();
        data.put("title", g_user.getName());
        data.put("body", body);
        data.put("receiverID", receiverIDs);
        data.put("senderID", g_user.id);
        data.put("sound","noti_sound.mp3");
        data.put("type", "reply");
        data.put("status", "normal");

        return mFunctions
                .getHttpsCallable("sendNotification")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                activity.hideHUD();

                                db.collection("notifications").document(noti.id).update("status", "sent")
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                ly.setVisibility(View.GONE);
                                            }
                                        });

                            }
                        });

                        //String result = (String) task.getResult().getData();
                        return "";
                    }
                });
    }


    private class ViewHolder{

        CircleImageView imv_sender;
        ImageView imv_photo, imv_map;
        TextView txv_title, txv_content, txv_date, txv_address;
        ImageButton imb_delete;
        Button btn_accept, btn_reject;
        RelativeLayout ryt_image;
        LinearLayout lyt_btn;
    }
}
