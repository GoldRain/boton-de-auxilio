package com.botondeauxilio.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.fragment.AddContactFragment;
import com.botondeauxilio.android.models.UserModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddContactListAdapter extends BaseAdapter {

    Context context;
    AddContactFragment fragment;
    ArrayList<UserModel> allUser = new ArrayList<>();
    ArrayList<UserModel> userModels = new ArrayList<>();
    LayoutInflater inflater;

    public AddContactListAdapter(Context context, AddContactFragment fragment){

        this.context = context;
        this.fragment = fragment;
        inflater = LayoutInflater.from(context);
    }

    public void setList(ArrayList<UserModel> send){

        this.userModels = send;
        /*allUser.clear();
        allUser = userModels;*/
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return allUser.size();
    }

    @Override
    public Object getItem(int position) {
        return allUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_add_contact_list, parent, false);

            viewHolder.imv_user = (CircleImageView)convertView.findViewById(R.id.imv_user);
            viewHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            viewHolder.txv_id_number = (TextView)convertView.findViewById(R.id.txv_id_number);
            viewHolder.btn_add_contact = (Button) convertView.findViewById(R.id.btn_add_contact);

            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        UserModel contact = allUser.get(position);

        Glide.with(context).load(contact.photoUrl).into(viewHolder.imv_user);
        viewHolder.txv_name.setText(contact.getName());
        viewHolder.txv_id_number.setText(contact.idNumber);
        viewHolder.btn_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.addContactRequest(contact);
            }
        });

        return convertView;
    }

    public void searchContact(String charText){

        charText = charText.toLowerCase();

        allUser.clear();

        if (charText.length() == 0){

            allUser.clear();

        } else {

            for (UserModel user : userModels){

                String name = user.getName().toLowerCase();
                String idNumber = user.idNumber.toLowerCase();

                if (name.contains(charText) || idNumber.contains(charText)) {
                    allUser.add(user);
                }
            }
        }

        notifyDataSetChanged();

    }

    public class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_id_number;
        Button btn_add_contact;
    }
}
