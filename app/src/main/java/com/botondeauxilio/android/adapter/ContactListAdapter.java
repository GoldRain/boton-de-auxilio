package com.botondeauxilio.android.adapter;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.botondeauxilio.android.R;
import com.botondeauxilio.android.activities.ContactManageActivity;
import com.botondeauxilio.android.fragment.ContactsFragment;
import com.botondeauxilio.android.models.UserModel;
import com.botondeauxilio.android.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactListAdapter extends BaseAdapter {

    ContactManageActivity context;
    ContactsFragment fragment;
    ArrayList<UserModel> contacts = new ArrayList<>();
    ArrayList<UserModel> allContact = new ArrayList<>();

    private LayoutInflater inflater;

    private int _cnt = 0;
    private int existCnt = Constants.g_user.contactID.size();

    SparseBooleanArray mSparseBooleanArray;

    public ContactListAdapter(ContactManageActivity activity, ContactsFragment fragment){

        this.context = activity;
        this.fragment = fragment;
        inflater = LayoutInflater.from(context);
    }

    public void setList(ArrayList<UserModel> contacts){

        this.contacts = contacts;
        allContact.clear();
        allContact.addAll(contacts);
        notifyDataSetChanged();
        mSparseBooleanArray = new SparseBooleanArray();
    }
    @Override
    public int getCount() {
        return allContact.size();
    }

    @Override
    public Object getItem(int position) {
        return allContact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){

            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_contact_list, parent, false);

            viewHolder.imv_user = (CircleImageView)convertView.findViewById(R.id.imv_user);
            viewHolder.txv_name = (TextView)convertView.findViewById(R.id.item_name);
            viewHolder.txv_id_number = (TextView)convertView.findViewById(R.id.item_id_number);
            viewHolder.checkBox = (CheckBox)convertView.findViewById(R.id.checkbox);

            convertView.setTag(viewHolder);
        }

        else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        UserModel user = (UserModel) allContact.get(position);

        Glide.with(context).load(user.photoUrl).into(viewHolder.imv_user);
        viewHolder.txv_name.setText(user.getName());
        viewHolder.txv_id_number.setText(user.idNumber);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //click false
                if (viewHolder.checkBox.isChecked()){
                    _cnt -= 1;
                    viewHolder.checkBox.setChecked(false);
                    mSparseBooleanArray.put(position, false);
                }
                else {
                    if ((_cnt + existCnt) < 5) {
                        _cnt += 1;
                        viewHolder.checkBox.setChecked(true);
                        mSparseBooleanArray.put(position, true);

                    } else {
                        context.showToast(R.string.have_5_contacts);
                    }
                }

            }
        });

        return convertView;
    }

    public ArrayList<UserModel> getCheckedContact() {

        ArrayList<UserModel> mTempArry = new ArrayList<>();

        for(int i = 0; i < allContact.size(); i++) {

            if (mSparseBooleanArray.get(i)) {
                mTempArry.add(allContact.get(i));
            }
        }

        return mTempArry;
    }

/*    public void searchContact(String charText){

        charText = charText.toLowerCase();

        allContact.clear();

        if (charText.length() == 0){

            allContact.clear();
            fragment.refreshLayout();
            *//*context.finish();
            context.startActivity(context.getIntent());*//*

        } else {

            for (UserModel user : contacts){

                String name = user.getName().toLowerCase();
                String idNumber = user.idNumber.toLowerCase();

                if (name.contains(charText) || idNumber.contains(charText)) {
                    allContact.add(user);
                }

                fragment.checkedUser = allContact;
            }
        }

        notifyDataSetChanged();

    }*/

    public class ViewHolder{

        CircleImageView imv_user;
        TextView txv_name, txv_id_number;
        CheckBox checkBox;
    }
}
