package com.botondeauxilio.android.utils;

public class Commons {

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf(".") > -1) {
            url = url.substring(0, url.lastIndexOf("."));
        }
        return url;
    }
}
