package com.botondeauxilio.android.utils;

import com.botondeauxilio.android.models.UserModel;

public class Constants {

    public static UserModel g_user = new UserModel();

    public static final int VOLLEY_TIME_OUT = 60000;

    public static boolean g_isAppRunning = false;
    public static final String PREF_KEY_TOKEN = "PREF_KEY_TOKEN";
    public static final int PROFILE_IMAGE_SIZE = 512;
    public static final int SPLASH_TIME = 1000;
    public static final String KEY_LOGOUT = "logout";
    public static final String KEY_TITLE = "title";

    //PREF KEYS
    public static final String PREFKEY_USEREMAIL = "user_email";
    public static final String PREFKEY_USERPWD = "user_pwd";

    public static final String KEY_COUNT = "count";
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int MY_REQUEST_CODE = 104 ;
    public static final int PICK_FROM_IMAGES = 116;
    public static final int KEY_CONTACT_CODE = 502;
    public static final int KEY_SIGNUP_CODE = 503;
    public static final String TOKEN = "token";
    public static final String KEY_CONTACTS = "key_contacts";
    public static final String KEY_EMAIL = "key_email";
    public static final String KEY_PWD = "key_password";
    public static double LATITUDE = 0;
    public static double LONGITUDE = 0;
    public static String NOTIFICATION = "notification";
    public static String KEY_IMAGE = "key_image";

}
