package com.botondeauxilio.android.models;

import java.util.ArrayList;

//accepted, sent user
public class RequestModel {

    public String id = "";
    public String receiverID = ""; //The user ID which sent to you
    public boolean status = false; //false : sent , true : accepted
    public String timeStamp = "";

    public RequestModel(){}
    public RequestModel(String id, String receiverID, boolean status, String timeStamp){

        this.id = id;
        this.receiverID = receiverID;
        this.status = status;
        this.timeStamp = timeStamp;
    }


    public RequestModel(String receiverID, boolean status, String timeStamp){

        //this.id = id;
        this.receiverID = receiverID;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    public RequestModel(String receiverID,String timeStamp){

        this.receiverID = receiverID;
        this.timeStamp = timeStamp;
    }
}
