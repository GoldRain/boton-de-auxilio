package com.botondeauxilio.android.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserModel implements Serializable {

    public String id = "";
    public String firstName = "";
    public String lastName = "";
    public String email = "";
    public String idNumber = "";
    public String phoneNumber = "";
    public String  photoUrl = "";
    public String token = "";
    public String timeStamp = "";
    public ArrayList<String> contactID = new ArrayList<>();
    public ArrayList<RequestModel> receivedID = new ArrayList<>();        //Received requests from other users(pending)
    public ArrayList<RequestModel> requestedID = new ArrayList<>(); //Sent request to other users(both of accepted status and sent status)

    public List<RequestModel> requests;
    public UserModel(){}

    public UserModel(String id, String firstName, String lastName, String email, String idNumber, String phoneNumber, String token, ArrayList<String> contactID, String photoUrl){

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.idNumber = idNumber;
        this.phoneNumber = phoneNumber;
        this.token = token;
        this.contactID = contactID;
        this.photoUrl = photoUrl;
    }

    public UserModel(String id, String firstName, String lastName, String email, String idNumber, String phoneNumber,
                     String token, ArrayList<String> contactID, String photoUrl, ArrayList<RequestModel> receivedID, ArrayList<RequestModel> requestedID){

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.idNumber = idNumber;
        this.phoneNumber = phoneNumber;
        this.token = token;
        this.contactID = contactID;
        this.photoUrl = photoUrl;
        this.receivedID = receivedID;
        this.requestedID = requestedID;
    }

    public UserModel(String id, String firstName, String lastName, String email, String idNumber,
                     String token, String photoUrl, String timeStamp){

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.idNumber = idNumber;
        this.token = token;
        this.photoUrl = photoUrl;
        this.timeStamp = timeStamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return firstName + " " + lastName;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
