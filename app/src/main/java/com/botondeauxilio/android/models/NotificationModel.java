package com.botondeauxilio.android.models;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import androidx.annotation.NonNull;

import com.botondeauxilio.android.BotondeAuxilioApplication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class NotificationModel {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public String id = "";
    public String title = "";
    public String content = "";
    public String date = "";
    public String receiverID = "";
    public String senderID = "";
    public double latitude = 0;
    public double longitude = 0;
    public String photoUrl = "";
    public String type = "";
    public String status = "";

    public NotificationModel(String id, String title, String content, String receiverID, String senderID,
                             String date, String type, String status){

        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.receiverID = receiverID;
        this.senderID = senderID;
        this.type = type;
        this.status = status;

    }

    public NotificationModel(String id, String title, String content, String receiverID, String senderID,
                             String date, String latitude, String longitude, String photoUrl, String type, String status){


        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.receiverID = receiverID;
        this.senderID = senderID;
        this.latitude = Double.parseDouble(latitude);
        this.longitude = Double.parseDouble(longitude);
        this.photoUrl = photoUrl;
        this.type = type;
        this.status = status;

    }


    public String getAddress() {

        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(BotondeAuxilioApplication.context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                //String addressLine = address.getAddressLine(0);
                result.append(address.getLocality());
            }
        } catch (IOException e) {
            Log.d("tag", e.getMessage());
        }

        return result.toString();
    }

}
